
package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name         = "RNMindWaveMobile"
  s.version      = package['version']
  s.summary      = "RNMindWaveMobile"
  s.description  = package['description']
  s.homepage     = "https://gitlab.com/aa900031/react-native-mindwave-mobile"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author       = package['author']
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitlab.com/aa900031/react-native-mindwave-mobile.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React-Core"
  #s.dependency "others"

end

  