
import { NativeModules, NativeEventEmitter } from 'react-native';

const { RNMindWaveMobile } = NativeModules;
const RNMindWaveMobileEventEmitter = new NativeEventEmitter(RNMindWaveMobile);

export default class {
  events = {
    didConnect: 'didConnect',
    didDisconnect: 'didDisconnect',
    deviceFound: 'deviceFound',
    eegPower: 'eegPower',
    eSense: 'eSense',
    eegBlink: 'eegBlink',
    mwmBaudRate: 'mwmBaudRate',
  }

  constructor() {
    RNMindWaveMobile.instance()
  }

  scan() {
    RNMindWaveMobile.scan()
  }

  connect(deviceId) {
    RNMindWaveMobile.connect(deviceId)
  }

  disconnect() {
    RNMindWaveMobile.disconnect()
  }

  onConnect(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.didConnect, callback)
  }

  onDisconnect(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.didDisconnect, callback)
  }

  onFoundDevice(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.deviceFound, callback)
  }

  onEEGPower(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.eegPower, callback)
  }

  onESense(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.eSense, callback)
  }

  onEEGBlink(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.eegBlink, callback)
  }

  onMWMBaudRate(callback) {
    RNMindWaveMobileEventEmitter.addListener(this.events.mwmBaudRate, callback)
  }

  removeAllListeners() {
    Object.keys(this.events).map(key => {
      const event = this.events[key]
      RNMindWaveMobileEventEmitter.removeAllListeners(event)
    })
  }

};

