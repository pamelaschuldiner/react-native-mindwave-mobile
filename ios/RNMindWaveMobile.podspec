
Pod::Spec.new do |s|
  s.name         = "RNMindWaveMobile"
  s.version      = "1.0.0"
  s.summary      = "RNMindWaveMobile"
  s.description  = <<-DESC
                  RNMindWaveMobile
                   DESC
  s.homepage     = "https://gitlab.com/aa900031/react-native-mindwave-mobile"
  s.license      = "MIT"
  s.author       = { "author" => "aa900031@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://gitlab.com/aa900031/react-native-mindwave-mobile", :tag => "master" }
  s.source_files  = "RNMindWaveMobile/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

